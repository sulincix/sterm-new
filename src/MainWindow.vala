public class MainWindow : Gtk.Window {
    private Gtk.Overlay overlay;
    public Gtk.Box main;
    public Gtk.Box window_box;
    public Gtk.Box tabbox;
    public TabView tabs;
    public Menu menu;
    public Gtk.Label blocker;
    public bool block_window_events = false;
    public GLib.Settings settings;
    private Gtk.Revealer window_box_revealer;
    private Gtk.Revealer window_resize_revealer;
    private Gtk.Revealer window_tabs_revealer;
    private Gtk.CssProvider css_provider;
    public NotifcationWidget notification;
    public string[] args;
    public MainWindow(string[] a){
        args = a;
        if(a.length > 1 && a[1] == "-e"){
            args = a[1:];
        }
        settings = new GLib.Settings("org.sulincix.sterm");
        theme_init();
        set_theme(settings.get_string("theme"));
        this.destroy.connect(Gtk.main_quit);

        css_provider = new Gtk.CssProvider ();
        Gtk.Settings.get_default().set("gtk-theme-name", "Adwaita-dark");
        Gtk.Settings.get_default().set("gtk-touchscreen-mode", false);
        Gtk.StyleContext.add_provider_for_screen (
            Gdk.Screen.get_default (),
            css_provider,
            Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        );

        // main box
        main = new Gtk.Box(Gtk.Orientation.VERTICAL,13);
        main.get_style_context().add_class("main_box");
        main.get_style_context().add_class("radius");

        // menu
        menu = new Menu(this);
        menu.halign = Gtk.Align.CENTER;
        menu.valign = Gtk.Align.CENTER;
        menu.hide();

        // blocker
        blocker = new Gtk.Label("");
        blocker.get_style_context().add_class("blocker");
        blocker.get_style_context().add_class("radius");

        // window box
        window_box_revealer = new Gtk.Revealer();
        window_resize_revealer = new Gtk.Revealer();
        window_tabs_revealer = new Gtk.Revealer();
        window_box = new Gtk.Box(Gtk.Orientation.HORIZONTAL,13);
        window_box_revealer.halign = Gtk.Align.END;
        window_box_revealer.valign = Gtk.Align.START;
        window_box_revealer.add(window_box);
        window_box_revealer.show_all();
        window_box.get_style_context().add_class("window_box");
        window_box_revealer.set_transition_type(Gtk.RevealerTransitionType.SLIDE_DOWN);
        window_box_revealer.set_transition_duration(250);
        show_window_box();

        // add window buttons
        ExitButton exit_button = new ExitButton();
        MoveButton move_button = new MoveButton();
        MinimizeButton minimize_button = new MinimizeButton();
        
        window_box.pack_end(exit_button,true,true,0);
        window_box.pack_end(move_button,true,true,0);
        window_box.pack_end(minimize_button,true,true,0);
        
        move_button.define_move_event(this);
        minimize_button.define_minimize_event(this);
        
        //resize button
        ResizeButton resize_button = new ResizeButton();
        resize_button.define_resize_event(this);
        window_resize_revealer.halign = Gtk.Align.END;
        window_resize_revealer.valign = Gtk.Align.END;
        window_resize_revealer.add(resize_button);
        window_resize_revealer.set_transition_type(Gtk.RevealerTransitionType.SLIDE_UP);
        window_resize_revealer.show_all();
        window_resize_revealer.set_transition_duration(250);
        
        //tabs
        tabs = new TabView(this);
        window_tabs_revealer.halign = Gtk.Align.START;
        window_tabs_revealer.valign = Gtk.Align.END;
        window_tabs_revealer.add(tabs.tabbox);
        window_tabs_revealer.set_transition_type(Gtk.RevealerTransitionType.SLIDE_UP);
        window_tabs_revealer.set_transition_duration(250);
        main.pack_start(tabs);
        
        // notification
        notification = new NotifcationWidget();
        notification.halign = Gtk.Align.CENTER;
        notification.valign = Gtk.Align.CENTER;
        
        // overlay
        overlay = new Gtk.Overlay();
        overlay.add_overlay(main);
        overlay.add_overlay(notification);
        overlay.add_overlay(blocker);
        overlay.add_overlay(window_box_revealer);
        overlay.add_overlay(window_tabs_revealer);
        overlay.add_overlay(window_resize_revealer);
        overlay.add_overlay(menu);

        var screen = this.get_screen();
        var visual = screen.get_rgba_visual();
        if(visual != null && screen.is_composited()){
            this.set_visual(visual);
        }

        this.add(overlay);
        // feature definition
        this.set_app_paintable(true);
        this.show_all();
        notification.hide();
        blocker.hide();
        menu.hide();
        this.set_size_request (800, 530);
        define_hotkeys();

        //notify events
        this.button_press_event.connect((event)=>{
            if(event.button == 3){
                if(!menu.status){
                    menu.open();
                    show_window_box();

                }else{
                    menu.close();
                }
            }
            return true;
        });
        string old_message = "";
        overlay.size_allocate.connect(()=>{
            Gtk.Allocation alloc;
            this.get_allocation(out alloc);
            string message = "%dx%d".printf(alloc.width,alloc.height);
            if (message != old_message){
                notification.show_text(message);
                old_message = message;
            }
        });
        hide_window_box();
    }
    
    public void create_tab(){
        string name = get_epoch().to_string();
        Terminal term;
        if(args.length <= 1){
            #if FLATPAK
            string[] shell_args = {"/bin/sh", "-c", "exec $SHELL"};
            #else
            string[] shell_args = { GLib.Environment.get_variable("SHELL") };
            #endif
            term = new Terminal(shell_args);
        }else{
            term = new Terminal(subarray(args,1,args.length));
        }
        term.name = name;
        term.connect_tabview(tabs);
        tabs.create_tab(term,name);
        tabs.set_active_tab(name);
        tabs.set_tab_title(name,"sterm");
        term.key_press_event.connect(hotkey_event);
        reload_theme();
    }
    
    public void set_color(int r, int g, int b, double a){
        string css_data = get_main_css();
        css_data += get_theme_css();
        css_data = css_data.replace("@r@",r.to_string());
        css_data = css_data.replace("@g@",g.to_string());
        css_data = css_data.replace("@b@",b.to_string());
        css_data = css_data.replace("@a@",a.to_string());
        try {
            css_provider.load_from_data(css_data);
        }catch (GLib.Error e) {
            stderr.printf("ERROR: %s\n", e.message);
        }
    }
    
    public void set_font_scale(double scale) {
        foreach(Gtk.Widget w in tabs.get_all_widgets()){
            var term = ((Terminal) ((Scrolled)w).widget);
            term.set_font_scale(scale);
        }    
    }
    
    public void reload_theme(){
        if(current_theme.name == "random"){
            ((theme_random)current_theme).reload();
        }
        block_window_events = settings.get_boolean("decoration");
        this.set_decorated(block_window_events);
        if(block_window_events){
            window_box_revealer.hide();
            window_resize_revealer.hide();
            main.get_style_context().remove_class("radius");
        }else{
            window_box_revealer.show();
            window_resize_revealer.show();
            main.get_style_context().add_class("radius");
        }
        
        
        set_font_scale(settings.get_int("font-scale") / 100.0);
        alpha = settings.get_int("alpha");
        foreach(Gtk.Widget w in tabs.get_all_widgets()){
            var term = ((Terminal) ((Scrolled)w).widget);
            if(current_theme.palette != null){
                term.set_colors(null, null, current_theme.palette);
                term.set_color(current_theme.bg_red, current_theme.bg_green, current_theme.bg_blue, alpha);
                term.set_foreground_color(current_theme.fg_red, current_theme.fg_green, current_theme.fg_blue, alpha);
            }
        }
        set_color(current_theme.bg_red, current_theme.bg_green, current_theme.bg_blue, alpha);
    }
    
    private bool isfullscreen = false;
    public bool lock_ui = false;
    public bool is_readonly = false;
    public void define_hotkeys(){
        // fullscreen
        var fs_event = new KeyEvent();
        fs_event.raw_keycode = KEY.RAW.F11;
        fs_event.event.connect(()=>{
            if(isfullscreen){
                this.unfullscreen();
                if(!block_window_events){
                    main.get_style_context().add_class("radius");
                }

                window_box_revealer.show();
                window_resize_revealer.show();
            }else{
                this.fullscreen();
                if(!block_window_events){
                    main.get_style_context().remove_class("radius");
                }
                window_box_revealer.hide();
                window_resize_revealer.hide();
            }
            isfullscreen = !isfullscreen;
        });
        add_hotkey_event(fs_event);
        // new tab event
        var docoration_event = new KeyEvent();
        docoration_event.keycode = KEY.E;
        docoration_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        docoration_event.event.connect(()=>{
            settings.set_boolean("decoration", !settings.get_boolean("decoration"));
            reload_theme();
        });
        add_hotkey_event(docoration_event);
        
        var readonly_event = new KeyEvent();
        readonly_event.raw_keycode = KEY.RAW.END;
        readonly_event.state = Gdk.ModifierType.MOD1_MASK;
        readonly_event.event.connect(()=>{
            change_readonly_mode();
        });
        add_hotkey_event(readonly_event);

        // new tab event
        var new_tab_event = new KeyEvent();
        new_tab_event.keycode = KEY.T;
        new_tab_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        new_tab_event.event.connect(()=>{
            create_tab();
        });
        add_hotkey_event(new_tab_event);
        
        // close tab event
        var close_tab_event = new KeyEvent();
        close_tab_event.keycode = KEY.W;
        close_tab_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        close_tab_event.event.connect(()=>{
            tabs.remove_tab(tabs.get_visible_child_name());
        });
        add_hotkey_event(close_tab_event);
        
        // reset event
        var reset_event = new KeyEvent();
        reset_event.keycode = KEY.R;
        reset_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        reset_event.event.connect(()=>{
            Terminal term = (Terminal) ((Scrolled)tabs.get_visible_child()).widget;
            term.reset(false,true);
            term.init();
        });
        add_hotkey_event(reset_event);

        

       // font bigger event
        var font_bigger_event = new KeyEvent();
        font_bigger_event.keycode = KEY.plus;
        font_bigger_event.state = Gdk.ModifierType.CONTROL_MASK ;
        font_bigger_event.event.connect(()=>{
            int scale = settings.get_int("font-scale");
            settings.set_int("font-scale", scale + 5);
            reload_theme();
        });
        add_hotkey_event(font_bigger_event);

       // font smaller event
        var font_smaller_event = new KeyEvent();
        font_smaller_event.keycode = KEY.minus;
        font_smaller_event.state = Gdk.ModifierType.CONTROL_MASK ;
        font_smaller_event.event.connect(()=>{
            int scale = settings.get_int("font-scale");
            settings.set_int("font-scale", scale - 5);
            reload_theme();
        });
        add_hotkey_event(font_smaller_event);

       // font reset event
        var font_reset_event = new KeyEvent();
        font_reset_event.keycode = KEY.zero;
        font_reset_event.state = Gdk.ModifierType.CONTROL_MASK ;
        font_reset_event.event.connect(()=>{
            settings.set_int("font-scale", 110);
            reload_theme();
        });
        add_hotkey_event(font_reset_event);

        // alpha decrease
        var alpha_decrease_event = new KeyEvent();
        alpha_decrease_event.raw_keycode = KEY.RAW.PGDN;
        alpha_decrease_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        alpha_decrease_event.event.connect(()=>{
            int alpha = settings.get_int("alpha") - 5;
            if (alpha < 15){
                alpha = 15;
            }
            settings.set_int("alpha", alpha);
            reload_theme();
        });
        add_hotkey_event(alpha_decrease_event);

        // alpha increase
        var alpha_increase_event = new KeyEvent();
        alpha_increase_event.raw_keycode = KEY.RAW.PGUP;
        alpha_increase_event.state = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.SHIFT_MASK;
        alpha_increase_event.event.connect(()=>{
            int alpha = settings.get_int("alpha") + 5;
            if (alpha > 255){
                alpha = 255;
            }
            settings.set_int("alpha", alpha);
            reload_theme();
        });
        add_hotkey_event(alpha_increase_event);

        // next tab event
        var next_tab_event = new KeyEvent();
        next_tab_event.raw_keycode = KEY.RAW.PGUP;
        next_tab_event.state = Gdk.ModifierType.CONTROL_MASK ;
        next_tab_event.event.connect(()=>{
            int index = tabs.get_tab_index(tabs.get_visible_child_name());
            if(index >= tabs.get_num_of_pages()-1){
                index = -1;
            }
            tabs.set_active_tab(tabs.get_tab_by_index(index + 1));
        });
        add_hotkey_event(next_tab_event);
        // prev tab event
        var prev_tab_event = new KeyEvent();
        prev_tab_event.raw_keycode = KEY.RAW.PGDN;
        prev_tab_event.state = Gdk.ModifierType.CONTROL_MASK ;
        prev_tab_event.event.connect(()=>{
            int index = tabs.get_tab_index(tabs.get_visible_child_name());
            if(index <= 0){
                index = tabs.get_num_of_pages();
            }
            tabs.set_active_tab(tabs.get_tab_by_index(index - 1));
        });
        add_hotkey_event(prev_tab_event);

        // tab switch keys
        for(int i=0;i<=9;i++){
            var set_tab_event = new KeyEvent();
            set_tab_event.raw_keycode = 9+i; // 1-9 buttons
            if(i == 0){
                set_tab_event.raw_keycode += 10; // fix 0
            }
            set_tab_event.state = Gdk.ModifierType.MOD1_MASK ;
            int index = (int)set_tab_event.raw_keycode - 10;
            set_tab_event.event.connect(()=>{
                string name = tabs.get_tab_by_index(index);
                tabs.set_active_tab(name);
            });
            add_hotkey_event(set_tab_event);
        }
    }
    public void change_readonly_mode(){
        tabs.window.is_readonly =! tabs.window.is_readonly;
        tabs.set_sensitive(!tabs.window.is_readonly);
        if(tabs.window.is_readonly){
            tabs.window.notification.show_text(_("Read Only"));
        }else{
            tabs.window.notification.show_text(_("Read & Write"));
        }
    }
    
    public void hide_window_box(){
        if(is_readonly){
            return;
        }
       if(window_box_revealer.get_child_revealed()){
            window_box_revealer.set_reveal_child(false);
            window_resize_revealer.set_reveal_child(false);
            window_tabs_revealer.set_reveal_child(false);
        }
    }
    public uint64 last_shown = get_epoch();
    public void show_window_box(){
        if(is_readonly){
            return;
        }
        if(!window_box_revealer.get_child_revealed()){
            last_shown = get_epoch();
            window_box_revealer.set_reveal_child(true);
            window_resize_revealer.set_reveal_child(true);
            window_tabs_revealer.set_reveal_child(true);
        }
    }
}
