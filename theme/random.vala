public class theme_random : color_theme {
    public void reload(){
        bg_red = GLib.Random.int_range(31,131);
        bg_green = GLib.Random.int_range(31,131);
        bg_blue =  GLib.Random.int_range(31,131);
        fg_red = 255-bg_green;
        fg_green = 255-bg_blue;
        fg_blue = 255-bg_red;
        palette = {
            randomcolor(), // black
            randomcolor(), // red
            randomcolor(), // green
            randomcolor(), // yellow
            randomcolor(), // blue
            randomcolor(), // magenta
            randomcolor(), // cyan
            randomcolor(), // white
            // bright
            randomcolor(), // black
            randomcolor(), // red
            randomcolor(), // green
            randomcolor(), // yellow
            randomcolor(), // blue
            randomcolor(), // magenta
            randomcolor(), // cyan
            randomcolor(), // white
        };    
    }

    public theme_random(){
        name = "random";
        reload();
        description = _("Randomized");

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid black;
          }
        ";

    }
    
    public Gdk.RGBA randomcolor(){
        var color = Gdk.RGBA();
        color.red = (double)GLib.Random.int_range(31,231)/255;
        color.blue = (double)GLib.Random.int_range(31,231)/255;
        color.green = (double)GLib.Random.int_range(31,231)/255;
        color.alpha = 1;
        return color;
    }
}
