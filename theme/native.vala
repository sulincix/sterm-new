public class theme_native : color_theme {
    public theme_native(){
        // create a hidden window
        var window = new Gtk.Window(Gtk.WindowType.TOPLEVEL);
        window.set_default_size(100, 100);
        window.hide(); // Hide the window
        var style_context = window.get_style_context();
        Gdk.RGBA background_color;
        background_color= style_context.get_background_color(Gtk.StateFlags.NORMAL);
        Gdk.RGBA foreground_color;
        foreground_color = style_context.get_color(Gtk.StateFlags.NORMAL);
    
        name = "native";
        description = _("Native");
        bg_red = (int)(background_color.red*255);
        bg_green = (int)(background_color.green*255);
        bg_blue =  (int)(background_color.blue*255);
        fg_red = (int)(foreground_color.red*255);
        fg_green = (int)(foreground_color.green*255);
        fg_blue = (int)(foreground_color.blue*255);

        palette = {
            background_color, // black
            color("#ff0000"), // red
            color("#00ff00"), // green
            color("#ffff00"), // yellow
            color("#0000ff"), // blue
            color("#ff00ff"), // magenta
            color("#00ffff"), // cyan
            foreground_color, // white
            // bright
            background_color, // black
            color("#aa0000"), // red
            color("#00aa00"), // green
            color("#aaaa00"), // yellow
            color("#0000aa"), // blue
            color("#aa00aa"), // magenta
            color("#00aaaa"), // cyan
            foreground_color, // white
        };


    }
}
