public class theme_adwaita_light : color_theme {
    public theme_adwaita_light(){
        name = "adwaita-light";
        description = _("Adwaita Light");
        bg_red = 222;
        bg_green = 222;
        bg_blue =  222;
        fg_red = 0;
        fg_green = 0;
        fg_blue = 0;
        palette = {
            color("#3D3846"), // black
            color("#C01C28"), // red
            color("#2EC27E"), // green
            color("#F5C211"), // yellow
            color("#1C71D8"), // blue
            color("#F511C2"), // magenta
            color("#11F5C2"), // cyan
            color("#F6F5F4"), // white
            // bright
            color("#3D3846"), // black
            color("#E01B24"), // red
            color("#33D17A"), // green
            color("#F6D32D"), // yellow
            color("#3584E4"), // blue
            color("#F622D3"), // magenta
            color("#22F6D3"), // cyan
            color("#FFFFFF"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid black;
          }
        ";

    }
}

public class theme_adwaita : theme_adwaita_light {
    public theme_adwaita(){
        name = "adwaita";
        description = _("Adwaita");
        bg_red = 53;
        bg_green = 53;
        bg_blue =  53;
        fg_red = 241;
        fg_green = 241;
        fg_blue = 241;


        palette = {
            color("#F6F5F4"), // black
            color("#C01C28"), // red
            color("#2EC27E"), // green
            color("#F5C211"), // yellow
            color("#1C71D8"), // blue
            color("#F511C2"), // magenta
            color("#11F5C2"), // cyan
            color("#3D3846"), // white
            // bright
            color("#FFFFFF"), // black
            color("#E01B24"), // red
            color("#33D17A"), // green
            color("#F6D32D"), // yellow
            color("#3584E4"), // blue
            color("#F622D3"), // magenta
            color("#22F6D3"), // cyan
            color("#3D3846"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid white;
          }
        ";
    }
}

