public class MenuButton : Gtk.Button {
    public MenuButton(string label){
        this.set_can_focus(false);
        this.get_style_context().add_class("transparent");
        this.set_relief(Gtk.ReliefStyle.NONE);
        this.set_label(label);
    }
}

public class ExitButton : Gtk.Button {

    public ExitButton(){
        this.clicked.connect((w)=>{
            Gtk.main_quit();
        });
        Gtk.Image i = new Gtk.Image();
        i.set_from_icon_name("sterm-exit-symbolic",Gtk.IconSize.LARGE_TOOLBAR);
        i.set_pixel_size(12);
        this.set_image(i);
        this.set_can_focus(false);

    }

}
public class MinimizeButton : Gtk.Button {

    public MinimizeButton(){
        Gtk.Image i = new Gtk.Image();
        i.set_from_icon_name("sterm-minimize-symbolic",Gtk.IconSize.LARGE_TOOLBAR);
        i.set_pixel_size(12);
        this.set_image(i);
        this.set_can_focus(false);
    }

    public void define_minimize_event(MainWindow window){
        this.clicked.connect(()=>{
            window.iconify();
        });
    }
}
public class MoveButton : Gtk.Button {

    public MoveButton(){
        Gtk.Image i = new Gtk.Image();
        i.set_from_icon_name("sterm-move-symbolic",Gtk.IconSize.LARGE_TOOLBAR);
        i.set_pixel_size(12);
        this.set_image(i);
        this.set_can_focus(false);
    }
    public void define_move_event(MainWindow window){
     // move button event
        int offset_x = -1;
        int offset_y = -1;
        uint64 last_click = get_epoch();
        this.clicked.connect((widget)=>{
            if (get_epoch() - last_click < 500){
                if(window.is_maximized){
                    window.unmaximize();
                    window.main.get_style_context().add_class("radius");
                }else{
                    window.maximize();
                    window.main.get_style_context().remove_class("radius");
                }
            }
            last_click = get_epoch();
        });
        var maximize_event = new KeyEvent();
        maximize_event.raw_keycode = KEY.RAW.ENTER;
        maximize_event.state = Gdk.ModifierType.MOD1_MASK;
        maximize_event.event.connect(()=>{
            if(window.is_maximized){
                window.unmaximize();
                if(!window.block_window_events){
                    window.main.get_style_context().add_class("radius");                 
                }
            }else{
                window.maximize();
                if(!window.block_window_events){
                    window.main.get_style_context().remove_class("radius");
                }
            }
        });
        add_hotkey_event(maximize_event);
        this.motion_notify_event.connect((event) => {
            if((event.state & Gdk.ModifierType.BUTTON1_MASK) == Gdk.ModifierType.BUTTON1_MASK){
                int w, h;
                Gtk.Allocation alloc;
                window.get_allocation(out alloc);
                w = alloc.width;
                h = alloc.height;
                if(offset_x < 0){
                    int window_x, window_y;
                    window.get_position(out window_x, out window_y);
                    offset_x = (int)(event.x_root - window_x);
                    offset_y = (int)(event.y_root - window_y);
                }
                window.move((int)(event.x_root-offset_x), (int)(event.y_root-offset_y));
                return true;
            }
            return false;
        });
        this.button_release_event.connect((event)=>{
             offset_x = -1;
             offset_y = -1;
             return false;
        });
    }
}

public class ResizeButton : Gtk.Button {

    public ResizeButton(){
        Gtk.Image i = new Gtk.Image();
        i.set_from_icon_name("sterm-resize-symbolic",0);
        i.set_pixel_size(4);
        this.set_image(i);
        this.set_can_focus(false);
        this.get_style_context().add_class("resize");
        this.set_relief(Gtk.ReliefStyle.NONE);
    }
    public void define_resize_event(MainWindow window){
     // move button event
        int offset_x = -1;
        int offset_y = -1;
        this.motion_notify_event.connect((event) => {
            if((event.state & Gdk.ModifierType.BUTTON1_MASK) == Gdk.ModifierType.BUTTON1_MASK){
                int w, h;
                Gtk.Allocation alloc;
                window.get_allocation(out alloc);
                w = alloc.width;
                h = alloc.height;
                int window_x = 0;
                int window_y = 0;
                window.get_position(out window_x, out window_y);
                if(offset_x < 0){
                    offset_x = (int)(w - event.x_root + window_x);
                    offset_y = (int)(h - event.y_root + window_y);
                }
                window.move(window_x, window_y);
                window.resize((int)(event.x_root + offset_x - window_x), (int)(event.y_root + offset_y - window_y));
                window.notification.show_text("%dx%d".printf(w,h));
                return true;
            }
            return false;
        });
        this.button_release_event.connect((event)=>{
             offset_x = -1;
             offset_y = -1;
             return false;
        });
    }
}
