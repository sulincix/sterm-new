namespace KEY{
    int C = 67;
    //int e = 101;
    int E = 69;
    int V = 86;
    int T = 84;
    int W = 87;
    int R = 82;
    int c = 99;
    int plus = 43;
    int minus = 45;
    int zero = 48;
    namespace RAW{
        int F11 = 95;
        int ENTER = 36;
        int HOME = 110;
        int END = 115;
        int PGUP = 112;
        int PGDN = 117;
    }
}


public class KeyEvent {
    public uint keycode = 0;
    public uint raw_keycode = 0;
    public Gdk.ModifierType state = 0;
    public bool blocked = true;
    public signal void event();
}

private KeyEvent[] events;

public bool hotkey_event(Gdk.EventKey event){
    int keycode=(int)event.keyval;
    int raw_keycode=(int)event.hardware_keycode;
    int state= (int)event.state % 256; // get first 8 digit
    foreach(KeyEvent e in events){
        #if DEBUG
        stderr.printf("r:%d c:%d s:%d == r:%d c:%d s:%d\n\n",raw_keycode, keycode, state, (int)e.raw_keycode, (int)e.keycode, e.state);
        #endif
        if(keycode == e.keycode || raw_keycode == e.raw_keycode){
            if ((state & e.state) == e.state){
                e.event();
                return e.blocked;
            }
        }
    }
    return false;
}

public void add_hotkey_event(KeyEvent e){
    if(events == null){
        events = {};
    }
    events += e;
}
