public class theme_dracula : color_theme {
    public theme_dracula(){
        name = "dracula";
        description = _("Dracula");
        bg_red = 40;
        bg_green = 42;
        bg_blue =  54;
        fg_red = 248;
        fg_green = 242;
        fg_blue = 242;
        palette = {
            color("#282a36"), // black
            color("#ff5555"), // red
            color("#50fa7b"), // green
            color("#f1fa8c"), // yellow
            color("#bd93f9"), // blue
            color("#ff79c6"), // magenta
            color("#8be9fd"), // cyan
            color("#f8f8f2"), // white
            // bright
            color("#394147"), // black
            color("#ff6666"), // red
            color("#61fb8c"), // green
            color("#f2fb9d"), // yellow
            color("#cea4fa"), // blue
            color("#ff90d7"), // magenta
            color("#9cf1fe"), // cyan
            color("#f9f9f3"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid black;
          }
        ";

    }
}
