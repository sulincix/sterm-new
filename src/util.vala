public int64 get_epoch(){
    return GLib.get_real_time () / 1000 ;
}

public string[] subarray(string[] array, int begin,int end){
    string[] ret = {};
    if(end>array.length){
         end = array.length;
    }
    for(int i=begin;i<end;i++){
        ret += array[i];
    }
    return ret;
}
