public class theme_hacker : color_theme {
    public theme_hacker(){
        name = "hacker";
        description = _("Hacker");
        bg_red = 0;
        bg_green = 0;
        bg_blue =  0;
        fg_red = 0;
        fg_green = 255;
        fg_blue = 0;
        palette = {
            color("#111111"), // black
            color("#00aa00"), // red
            color("#00aa00"), // green
            color("#00aa00"), // yellow
            color("#00aa00"), // blue
            color("#00aa00"), // magenta
            color("#00aa00"), // cyan
            color("#ffffff"), // white
            // bright
            color("#111111"), // black
            color("#00ff00"), // red
            color("#00ff00"), // green
            color("#00ff00"), // yellow
            color("#00ff00"), // blue
            color("#00ff00"), // magenta
            color("#00ff00"), // cyan
            color("#ffffff"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid #00ff00;
          }
        ";

    }
}
