public class theme_light : color_theme {
    public theme_light(){
        name = "light";
        description = _("Light");
        bg_red = 255;
        bg_green = 255;
        bg_blue = 255;
        fg_red = 0;
        fg_green = 0;
        fg_blue = 0;
    }
}

public class theme_darker : theme_light {
    public theme_darker(){
        name = "darker";
        description = _("Darker");
        css = "
         .notification,
         .window_box {
             background: rgba(0, 0, 0, 0.89);
             border: none;
         }
         * {
             color: white;
         }
        ";
    }
}

public class theme_lighter : color_theme {
    public theme_lighter(){
        name = "lighter";
        description = _("Lighter");
        css = "
         .notification,
         .window_box {
             background: rgba(255, 255, 255, 0.89);
             border: none;
         }
         * {
             color: black;
         }
        ";
    }
}
