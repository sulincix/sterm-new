public class theme_solarized : color_theme {
    public theme_solarized(){
        name = "solarized";
        description = _("Solarized Dark");
        bg_red = 0;
        bg_green = 43;
        bg_blue = 54;
        fg_red = 147;
        fg_green = 161;
        fg_blue = 161;
        palette = {
            color("#184753"),
            color("#dc322f"),
            color("#859900"),
            color("#b58900"),
            color("#268bd2"),
            color("#d33682"),
            color("#2aa198"),
            color("#eee8d5"),
            // bright
            color("#295864"),
            color("#ed4340"),
            color("#96aa11"),
            color("#c69aff"),
            color("#379ce3"),
            color("#e44793"),
            color("#3bb2a9"),
            color("#fff9e6"),
        };
        
        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid white;
          }
        ";
    }
}

public class theme_solarized_light : theme_solarized {
    public theme_solarized_light(){
        name = "solarized_light";
        description = _("Solarized Light");
        bg_red = 238;
        bg_green = 232;
        bg_blue = 213;
        fg_red = 7;
        fg_green = 54;
        fg_blue = 66;
        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid black;
          }
        ";
    }
}
