public class Menu : Gtk.Box {
    private MainWindow main;
    public Gtk.Stack stack;
    public bool status;
    public Menu(MainWindow m){
        main = m;
        status = false;
        stack = new Gtk.Stack();
        stack.set_transition_type(Gtk.StackTransitionType.SLIDE_LEFT_RIGHT);
        stack.set_transition_duration(250);
        var menubox = new Gtk.Box(Gtk.Orientation.VERTICAL,0);
        this.get_style_context().add_class("notification");
        this.set_orientation(Gtk.Orientation.VERTICAL);
        // newtab button
        var newtab = new MenuButton(_("New Tab"));
        newtab.clicked.connect((w)=>{
            close();
            m.create_tab();
        });
        menubox.pack_start(newtab,true,true,0);
         // copy button
        var copy = new MenuButton(_("Copy"));
        copy.clicked.connect(copy_clipboard_event);
        menubox.pack_start(copy,true,true,0);
        // paste button
        var paste = new MenuButton(_("Paste"));
        paste.clicked.connect(paste_clipboard_event);
        menubox.pack_start(paste,true,true,0);
        // closetab button
        var closetab = new MenuButton(_("Close Tab"));
        closetab.clicked.connect((w)=>{
            close();
            main.tabs.remove_tab(main.tabs.get_visible_child_name());
        });
        menubox.pack_start(closetab,true,true,0);
        // reset button
        var reset = new MenuButton(_("Reset Terminal"));
        reset.clicked.connect((w)=>{
            close();
            Terminal t = get_current();
            if(t == null){
                return;
            }
            t.reset(false, true);
            t.init();
        });
        menubox.pack_start(reset,true,true,0);
        // RO RW button
        var readonly = new MenuButton(_("Read Only Mode"));
        readonly.clicked.connect((w)=>{
            main.change_readonly_mode();
            if (main.is_readonly){
                readonly.set_label(_("Read & Write Mode"));
            } else {
                readonly.set_label(_("Read Only Mode"));
            }
            close();
        });
        // Themes button
        var but_theme = new MenuButton(_("Themes"));
        but_theme.clicked.connect((w)=>{
            stack.set_visible_child_name("themes");
        });
        menubox.pack_start(but_theme,true,true,0);
        // theme selection list
        var themebox = new Gtk.Box(Gtk.Orientation.VERTICAL,0);
        var themescroll = new Gtk.ScrolledWindow(null, null);
        themescroll.set_size_request(200,400);
        var but_back = new MenuButton("<"+_("Go Back")+">");
        but_back.clicked.connect((w)=>{
            stack.set_visible_child_name("menu");
        });
        themebox.pack_start(but_back,true,true,0);
        foreach(color_theme t in color_themes){
            var tset = new MenuButton(t.description);
            var name = t.name;
            tset.clicked.connect((b)=>{
                main.settings.set_string("theme",name);
                set_theme(name);
                main.reload_theme();
            });
            themebox.pack_start(tset,true,true,0);
        }
        this.pack_start(stack, true, true,0);
        themescroll.add(themebox);
        stack.add_titled(menubox, "menu", "menu");
        stack.add_titled(themescroll, "themes", "themes");
   }
    public Terminal get_current(){
        Terminal term = (Terminal) ((Scrolled)main.tabs.get_visible_child()).widget;
        return term;
    }


    public void open(){
        this.show();
        main.notification.blocked = true;
        main.tabs.set_sensitive(false);
        main.notification.hide_text();
        main.blocker.show();
        this.grab_focus();
        status = true;
        main.show_window_box();
    }
    public void close(){
        this.hide();
        main.notification.blocked = false;
        main.tabs.set_sensitive(true);
        main.blocker.hide();
        get_current().grab_focus();
        status = false;
        stack.set_visible_child_name("menu");
        main.hide_window_box();
    }

    // events
    public void copy_clipboard_event(Gtk.Widget w){
        Terminal t = get_current();
        if(t == null){
            return;
        }
        t.copy_clipboard();
        this.close();
        main.notification.show_text(_("Copy to clipboard"));
    }
    public void paste_clipboard_event(Gtk.Widget w){
        Terminal t = get_current();
        if(t == null){
            return;
        }
        t.paste_clipboard();
        this.close();
        main.notification.show_text(_("Paste from clipboard"));
    }
}
