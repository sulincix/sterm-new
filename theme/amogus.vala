public class theme_amogus : color_theme {
    public theme_amogus(){
        name = "amogus";
        description = _("Amogus");
        bg_red = 1;
        bg_green = 1;
        bg_blue =  1;
        fg_red = 247;
        fg_green = 247;
        fg_blue = 247;
        palette = {
            color("#313131"), // black
            color("#f21717"), // red
            color("#17f217"), // green
            color("#ffde2a"), // yellow
            color("#235685"), // blue
            color("#f217f2"), // magenta
            color("#75dbf4"), // cyan
            color("#f2f2f2"), // white
            // bright
            color("#424242"), // black
            color("#f32828"), // red
            color("#28f328"), // green
            color("#ffef3b"), // yellow
            color("#345696"), // blue
            color("#f328f3"), // magenta
            color("#86ecf5"), // cyan
            color("#f3f3f3"), // white
        };

        css = "
          .notification,
          .window_box,
          .main_box {
            border: 1px solid #171717;
          }
        ";

    }
}

